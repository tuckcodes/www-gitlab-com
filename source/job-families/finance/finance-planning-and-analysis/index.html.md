---
layout: job_family_page
title: "Financial Planning and Analysis"
---

## Levels

## Intermediate

## Senior

## Manager, FP&A

You will be working directly with the CFO and executive team leaders across the company to enhance and improve our forecasting and business modeling capabilities.  You will also be working with our data and analytics team to help develop predictive modeling and decision support capabilities.

For this role, you should hold a degree in Finance or Accounting followed by relevant work experience in the software/SaaS industry. Knowledge of forecasting models and cost accounting processes are key requirements for this position. You will explore investment options and set company-wide financial policies. You will manage a team of individuals who are highly skilled at data analysis and facilitating the corporate planning and reporting function.

Ultimately, you will ensure our financial planning is healthy and profitable and aligns with business objectives.

### Responsibilities

- Create a fully integrated financial model that produces a highly accurate monthly forecast of an income statement, balance sheet and cash flows and allows for real team scenario generation compared against baseline models.
- Implement company wide headcount forecasting and requisition process.
- Prepare cost projections
- Analyze and report on current financial status
- Conduct thorough research of historical financial data including detailed benchmarking analysis against industry comparables.
- Explore investment options and present risk and opportunities
- Coordinate with the CFO and the executive team on long-term financial planning
- Compare anticipated and actual results and identify areas of improvement
- Coordinate quarterly forecasting process working with executive team members and direct reports.
- Review accounting transactions for data accuracy
- Establish financial policies and document them in the GitLab handbook.
- Prepare visualization of financial data to promote internal and external understanding of the company’s financial results.
- Build and manage a highly productive team focused on aligning the planning process with KPIs.

### Requirements

- Proven work experience as a Senior Financial Analyst, Financial Analyst or similar role
- Four years of experience with enterprise software of SaaS business models.
- Curiosity and the desire to continually grow and improve.
- A passion for data and for building scalable and sustainable systems.
- Hands-on experience with financial and visualization software
- Expertise in Google sheets (we do not use excel for modeling purposes)
- Consistent track record of using quantitative analysis to impact key business decisions.
- Excellent analytical skills
- Ability to present financial data using detailed reports and charts
- Demonstrable strategic thinking skills
- Confidentiality in handling sensitive financial information
- BS degree in Finance, Accounting or Economics.  Additional work in mathematics or statistics would be helpful.
- 5+ years of experience building/automating/exploring analyses in any of SQL, R, Python, or highly scalable excel/VBA.
- Relevant certification (e.g. CFA/CPA) is a plus

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Senior Director, Data & Analytics
- Next, candidates will be invited to schedule a 45 minute interview with our CFO
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Next, candidates will be invited to schedule a 45 minute interview with our CCO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our hiring page.
