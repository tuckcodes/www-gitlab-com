layout: markdown_page
title: "All-Remote Meetings"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

GitLab is an all-remote company with [team members](/company/team/) located in more than 57 countries around the world. 
Check out the [main all-remote page](/company/culture/all-remote/) to learn more. 

On this page, we're detailing how to optimize meetings in an all-remote environment.

## How do you do all-remote meetings right? 

"How do you do meetings right?" is a common question asked of all-remote companies like GitLab.

The truth is that much of the same advice applicable to in-person meetings apply to meetings within an all-remote company, with a few notable distinctions.

### Make meetings optional

When you work in a global all-remote company, the usual assumptions about availability are opposite the norm. We have a growing team working in over 50 countries, with many time zones covered, which makes synchronous meetings impractical, burdensome, and inefficient. Anyone who has worked in a corporate environment has likely seen the sarcastic "I Survived Another Meeting That Should Have Been An Email" award. As an all-remote company, we do not look to a meeting by default, and we strive to make meetings optional.

In many companies, meetings are used as a mechanism to create consensus. As you'll read in the [Leadership](/handbook/leadership/) portion of GitLab's handbook, we are not a democratic or consensus driven company. People are encouraged to give their comments and opinions, but in the end one person decides the matter after they have listened to all the feedback.

This works because of our values, which leads GitLab to hire individuals who enjoy being a manager of one, a point detailed in our [Efficiency value](/handbook/values/#efficiency).

### Have an agenda

Not all meetings are inherently bad. We encourage managers to establish regular 1:1 meetings with their team, for example. Many meetings can be avoided by understanding how to [work well asynchronously](/2015/04/08/the-remote-manifesto/), a topic we'll cover in more detail on a separate page. GitLab has a [documented approach to efficient, productive 1:1s](/handbook/leadership/1-1/) that we welcome other companies to implement, and contribute to if they have suggestions for improvement. Below is a video overview of executing 1:1 meetings.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/KUxxjGJv1dQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

If you determine that a meeting is needed to move a project forward, address a blocker, or resolve a miscommunication, be sure to have an agenda.
* Create the agenda in a Google Doc ahead of time
* Link the agenda in the meeting invite
* Establish the agenda as far in advance of the meeting as possible
* Agendas should be simple, bulleted lists that are concise and direct — a [boring solution](/handbook/values/#boring-solutions).

### Document everything (yes, everything)

It's not rude to focus on documentation in a meeting. A surefire way to waste time in a meeting is to avoid writing anything down. Meetings within an all-remote company require documentation to be worthwhile.

* During the meeting, add input and feedback from attendees to existing agenda items.
* For action items, we [go directly to a GitLab issue](/2016/03/03/start-with-an-issue/). This creates a direct takeaway from the meeting, where ideas are summarized and action can begin immediately.
* For optional attendees, or key team members who could not attend the meeting live, tagging them in the resulting GitLab issue(s) enables them to get themselves up to speed and contribute when it is suitable for their schedule.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/N3COqJvme-Q?start=41" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

You can see this in practice by viewing past [GitLab Group Conversations](/handbook/people-operations/group-conversations/) on our [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/search?query=group+conversation).

### Cancel unnecessary recurring meetings

Recurring meetings are oftentimes established as meaningful points along a given a journey. Don't hesitate to cancel them after their purpose has been served. Cancelling meetings isn't a slight to those on the invite list. In fact, riding multiple calendars of a meeting should be celebrated and conjure a sense of liberation.

### Use the right tools

All-remote meetings are made simpler given that there's no jockeying for space in a boardroom, scrounging for huddle rooms, or wondering if a given group still needs the meeting room they've reserved — all very real conundrums in colocated environments.

* GitLab uses Zoom for video calls and screensharing. Its simple recording function makes it easy to capture meetings for others to watch at a later time. Learn more about how we optimize Zoom usage in our meetings in the [Tools and Tips](/handbook/tools-and-tips/#zoom) portion of our Handbook.
* We use [GitLab Issues](https://docs.gitlab.com/ee/user/project/issues/) to document action items that come out of any given meeting, and loop anyone else in who opted out of real-time attendance.
* Leverage tools such as Calendly, which can show you as busy in chat tools like Slack. 

### Meetings are about the work, not the background

One's appearance, surroundings, and background can be the source of great stress and anxiety when preparing for a video call. At GitLab, we encourage team members to bring their whole selves to work.

* Don't waste time trying to find the perfect backdrop for your video call.
* Celebrate [unique surroundings](/2019/06/25/how-remote-work-at-gitlab-enables-location-independence/). It's not uncommon to see GitLab team members participate in a video call from the the shores of a lake, coffee shops, RVs, or even while walking.
* Focus on your internet connection and your audio quality ([use headphones](/2019/06/28/five-things-you-hear-from-gitlab-ceo/)), reducing listening fatigue for others.
* Encourage others to say hello! All-remote employees invite others into their homes via video call, creating a unique opportunity to share their lives with colleagues.
* Consider *not* using a video call. Visit GitLab's [Communication](/handbook/communication/#video-calls) section of the Handbook to learn more.

----

Return to the main [all-remote page](/company/culture/all-remote/).
